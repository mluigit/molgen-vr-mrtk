﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class SettingsManager : MonoBehaviour {

    private string _userID;
    private MoleculeCountPreset _preset;
    public GameObject HandMenu;
    public GameObject ControllerHandMenu;

    public TextMeshProUGUI inputField;

    public void SetUserID(TMP_InputField input)
    {
        _userID = input.text;
        
    }

    public void SetPreset(MoleculeCountPreset preset)
    {
        _preset = preset;
    }

	public void StartSession()
    {
        if (_userID == "" || _preset == null)
            return;

        AnalyticsTracker.Instance.userID = _userID;
        AnalyticsTracker.Instance.NewSession();
        AnalyticsTracker.Instance.LogItem(DateTime.Now, "Application", "Start Session");
        MoleculeManager.Instance.SetMoleculeCountPreset(_preset);
        HandMenu.SetActive(true);
        ControllerHandMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}

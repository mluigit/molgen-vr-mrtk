﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Spawn Actions/Spawn RNA")]
public class SpawnRNA : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine molStateMachine = stateMachine as MoleculeStateMachine;

        if (molStateMachine == null)
            return;

        mRNAObjectPooler pooler = FindObjectOfType<mRNAObjectPooler>();

        pooler.SpawnmRNA(molStateMachine, null, null);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Molecule/Translation Group")]
public class TranslationGroup : ScriptableObject {

    public Molecule mRNA;
    public Molecule[] proteins;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Contrain")]
public class ContrainAction : BindAction{

    public bool contrain;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine molecule, Transform activeSite)
    {
        Rigidbody rigidbody = moleculeStateMachine.GetComponent<Rigidbody>();

        if(contrain)
            rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        else
            rigidbody.constraints = RigidbodyConstraints.None;
    }
}

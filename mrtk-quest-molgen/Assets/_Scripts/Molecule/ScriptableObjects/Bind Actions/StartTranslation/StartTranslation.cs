﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Start Translation")]
public class StartTranslation : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        RibosomeController controller = moleculeStateMachine.GetComponent<RibosomeController>();
        if (controller != null)
            controller.StartTranslation(moleculeStateMachine, boundMolecule);
    }
}

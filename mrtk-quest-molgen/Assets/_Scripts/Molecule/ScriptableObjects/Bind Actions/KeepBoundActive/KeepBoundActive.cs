﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Keep Bound Active")]
public class KeepBoundActive : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        boundMolecule.Binding = false;
        boundMolecule.ChangeState(MoleculeManager.Instance.freeState);
    }
}

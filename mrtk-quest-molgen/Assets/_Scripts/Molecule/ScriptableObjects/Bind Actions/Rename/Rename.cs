﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Rename")]
public class Rename : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine molecule, Transform activeSite)
    {
        moleculeStateMachine.label.SetLabel(molecule.molecule.name);
    }
}

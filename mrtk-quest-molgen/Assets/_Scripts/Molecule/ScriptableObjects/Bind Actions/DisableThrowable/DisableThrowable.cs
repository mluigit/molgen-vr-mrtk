﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR.InteractionSystem;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Disable Throwable")]
public class DisableThrowable : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        //Interactable interactable = boundMolecule.GetComponent<Interactable>();
        //Throwable throwable = boundMolecule.GetComponent<Throwable>();
        Rigidbody rigidbody = boundMolecule.GetComponent<Rigidbody>();

        //if (throwable == null)
            //return;

        //interactable.attachEaseIn = false;
        //interactable.enabled = false;
        //throwable.attachmentFlags = 0;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;

    }
}

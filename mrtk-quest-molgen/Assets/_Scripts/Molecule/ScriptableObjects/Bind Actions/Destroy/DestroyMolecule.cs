﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Destroy Molecule")]
public class DestroyMolecule : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        Destroy(moleculeStateMachine.gameObject);
    }
}

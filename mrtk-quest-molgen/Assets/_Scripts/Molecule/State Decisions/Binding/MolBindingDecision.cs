﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Decisions/Binding")]
public class MolBindingDecision : StateDecision {

    public bool isBinding;

    public override bool Decide(StateMachine stateMachine)
    {
        MoleculeStateMachine molStateMachine = stateMachine as MoleculeStateMachine;

        if (molStateMachine == null)
        {
            Debug.Log(stateMachine.currentState + " is attempting to make a decision on a StateMachine other than MoleculeStateMachine.");
            return false;
        }

        if (molStateMachine.Binding == isBinding)
            return true;

        return false;
    }
}

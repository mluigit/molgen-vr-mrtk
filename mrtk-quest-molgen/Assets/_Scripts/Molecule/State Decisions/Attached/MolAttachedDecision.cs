﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Decisions/Attached")]
public class MolAttachedDecision : StateDecision {

    public bool isAttached;

    public override bool Decide(StateMachine stateMachine)
    {
        MoleculeStateMachine molStateMachine = stateMachine as MoleculeStateMachine;

        if (molStateMachine == null)
        {
            Debug.Log(stateMachine.currentState + " is attempting to make a decision on a StateMachine other than MoleculeStateMachine.");
            return false;
        }

        if (molStateMachine.Attached == isAttached)
            return true;

        return false;
    }
}

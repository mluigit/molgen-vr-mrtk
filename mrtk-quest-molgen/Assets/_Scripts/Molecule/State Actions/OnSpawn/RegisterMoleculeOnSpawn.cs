﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Actions/Register on Spawn")]
public class RegisterMoleculeOnSpawn : StateAction {

    public State freeState;

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;
        if (moleculeStateMachine == null)
            return;

        MoleculeManager moleculeManager = FindObjectOfType(typeof(MoleculeManager)) as MoleculeManager;
        if (moleculeManager == null)
            return;

        moleculeManager.AddMolecule(moleculeStateMachine);

        //change to free state
        //moleculeStateMachine.ChangeState(freeState);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/State Action/Stop Expanding Radius")]
public class StopExandingRadius : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;
        if (moleculeStateMachine != null)
            moleculeStateMachine.StopExpandingRadius();
    }
}

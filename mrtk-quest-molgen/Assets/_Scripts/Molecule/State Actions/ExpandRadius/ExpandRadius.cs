﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/State Action/Expand Radius")]
public class ExpandRadius : StateAction {

    public float delay = 10f;

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;
        if (moleculeStateMachine != null)
            moleculeStateMachine.ExpandRadius(delay);
    }
}

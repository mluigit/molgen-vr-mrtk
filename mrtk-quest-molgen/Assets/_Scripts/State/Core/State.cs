﻿using UnityEngine;


    [CreateAssetMenu(menuName = "States/State")]
    [System.Serializable]
    public class State : ScriptableObject
    {
        //ACTIONS
        public StateAction[] onEnterActions;
        public StateAction[] onExecuteActions;
        public StateAction[] onExitActions;

        //TRANSITIONS
        public StateTransition[] transitions;


        //PUBLIC METHODS
        public void Enter(StateMachine _stateMachine)
        {
            DoActions(onEnterActions, _stateMachine);
        }

        public void Execute(StateMachine _stateMachine)
        {
            EvalutateTransitions(_stateMachine);
            DoActions(onExecuteActions, _stateMachine);
        }

        public void Exit(StateMachine _stateMachine)
        {
            DoActions(onExitActions, _stateMachine);
        }

        //calls abstract class Act and passes the state machine that called this state as an argument
        private void DoActions(StateAction[] _actions, StateMachine _stateMachine)
        {
            if (_actions.Length < 1)
                return;
            
            for (int i = 0; i < _actions.Length; i++)
            {
                if (_actions[i] == null)
                {
                    Debug.Log("Missing action on " + this.name);
                    continue;
                }
                    
                _actions[i].Act(_stateMachine);
            }
        }

        //evaluates whether the state machine should stay in its current state or transition to a new state
        private void EvalutateTransitions(StateMachine _stateMachine)
        {
            if (transitions.Length < 1)
                return;
            
            for (int i = 0; i < transitions.Length; i++)
            {
                //null reference checks
                if (transitions[i].decision == null)
                {
                    Debug.Log("Missing decision on " + this.name);
                    continue;
                }

                if(transitions[i].trueState == null)
                {
                    Debug.Log("Missing true state on " + transitions[i].decision.name + " on " + this.name);
                    continue;
                }

                //evaluates conditions defined in each StateDecision and passes the state machine that called this state as an argument
                bool shouldTransition = transitions[i].decision.Decide(_stateMachine);
                if (shouldTransition)
                {
                    _stateMachine.ChangeState(transitions[i].trueState); //calls state change to true state
                }
            }
        }
    }


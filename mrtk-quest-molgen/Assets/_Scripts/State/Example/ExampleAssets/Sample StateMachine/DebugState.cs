﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugState : MonoBehaviour {

	public void PrintCurrentState(StateMachine stateMachine)
    {
        Debug.Log(stateMachine.name.ToString() + " is currently in its " + stateMachine.CurrentState().ToString() + ".");
    }
}

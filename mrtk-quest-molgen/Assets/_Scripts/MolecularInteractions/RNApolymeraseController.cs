﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RNApolymeraseController : MonoBehaviour
{

    //the waypoints to follow
    public WaypointManager.WaypointGroup waypointGroup;
    //how fast to move
    public float speed = 1f;
    [Range(0f, 1f)]
    public float bindProbability = 0.5f; //the probability of the molecule not disassociating when the CAP site is not bound to CRP
    public Molecule mRNAmolecule;

    private Transform[] _path;
    private Transform _transform;
    private IEnumerator _moveToPoint;
    private MoleculeManager _molManager;
    private WaypointManager _waypointManager;
    private mRNAController _mRNAcontroller;

    private void Awake()
    {
        _molManager = FindObjectOfType(typeof(MoleculeManager)) as MoleculeManager;
        _waypointManager = FindObjectOfType(typeof(WaypointManager)) as WaypointManager;
        _transform = transform;
    }

    public void SetGroup(WaypointManager.WaypointGroup group)
    {
        if (group == WaypointManager.WaypointGroup.LAC_FULL && MoleculeManager.Instance.Repressed)
        {
            waypointGroup = WaypointManager.WaypointGroup.LAC;
        }  
        else
        {
            waypointGroup = group;
        }
            

        _path = _waypointManager.GetPointsByGroup(waypointGroup);
    }

    public void StartMoving()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        Debug.Log("RNA Poly Is Kin");
        GoToNextWaypoint(0);
        //attachment fix test
    }

    public void GoToNextWaypoint(int index)
    {
        if (_moveToPoint != null)
            StopCoroutine(_moveToPoint);

        _moveToPoint = C_MoveToWayPoint(index);
        StartCoroutine(_moveToPoint);
    }

    IEnumerator C_MoveToWayPoint(int index)
    {
        Vector3 start = _transform.position;
        Vector3 point = _path[index].position;
        float startTime = Time.time;

        while(Time.time-startTime < speed)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            _transform.position = Vector3.Lerp(start, point, (Time.time - startTime) / speed);
            //_transform.position = Vector3.Lerp(_transform.position, point, Time.fixedDeltaTime);
            _transform.rotation = Quaternion.Lerp(_transform.rotation, Quaternion.LookRotation(point - _transform.position, _transform.up), (Time.time - startTime) / speed);

            if (_mRNAcontroller != null)
            {
                _mRNAcontroller.Grow();
            }
                
            yield return null;
        }

        /*
        while(Vector3.Distance(_transform.position, point) > 0.01f)
        {
            _transform.position = Vector3.Lerp(_transform.position, point, Time.fixedDeltaTime * speed);
            _transform.rotation = Quaternion.Lerp(_transform.rotation, Quaternion.LookRotation(point - _transform.position, _transform.up), Time.fixedDeltaTime * speed);
            yield return null;
        }
        */
        _transform.position = point;

        if(index < _path.Length -1)
        {
            int next = index + 1;
            GoToNextWaypoint(next);
        }
        else
        {
            EndMoving();
        }
    }

    public void EndMoving()
    {
        
        SpawnmRNA();
        transform.SetParent(null);
        StartCoroutine(ResetSequence());
    }

    public void ForceReset()
    {
        StartCoroutine(ResetSequence());
    }

    IEnumerator ResetSequence()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        yield return new WaitForSeconds(1f);
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().AddForce(new Vector3(-1,1,0) * 750f);
        yield return new WaitForSeconds(1f);
        GetComponent<Rigidbody>().isKinematic = false;
        Debug.Log("RNA Poly Reset");
        GetComponent<MoleculeStateMachine>().ResetMolecule();
    }
    public void FinalMove(){
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().AddForce(new Vector3(-1,1,0) * 2f);
    }

    //growth
    public void SetmRNA(mRNAController mRNA, Molecule mol)
    {
        _mRNAcontroller = mRNA;
        _mRNAcontroller.isGrowing = true;
        mRNAmolecule = mol;
        _mRNAcontroller.UpdateMaterial(mol.material);
    }

    //release mRNA
    private void SpawnmRNA()
    {
        /*
        GameObject newMolecule;

        newMolecule = Instantiate(_molManager.moleculePrefab, transform.position, transform.rotation);

        MoleculeStateMachine newMolStateMachine = newMolecule.GetComponent<MoleculeStateMachine>();
        newMolStateMachine.SetMolecule(mRNAmolecule);
        */
        MoleculeStateMachine newMolStateMachine = _molManager.SpawnMolecule(mRNAmolecule, transform);

        if (_mRNAcontroller == null)
            return;
        MeshRenderer renderer = newMolStateMachine.modelParent.GetComponentInChildren<MeshRenderer>();
        if (renderer != null)
            renderer.material = _mRNAcontroller.material;
        _mRNAcontroller.handle.transform.SetParent(newMolStateMachine.modelParent, false);
        _mRNAcontroller.isGrowing = false;
        _mRNAcontroller = null;
    }
}

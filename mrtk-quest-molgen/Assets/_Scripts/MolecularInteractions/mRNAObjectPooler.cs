﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mRNAObjectPooler : MonoBehaviour {

    public List<mRNAController> mRNAControllers;
    private List<mRNAController> activeControllers;

    private void Start()
    {
        activeControllers = new List<mRNAController>();
    }

    public void SpawnmRNA(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Molecule mol)
    {
        //get mRNA Controller
        mRNAController newController = AvailableController();

        if (newController == null)
        {
            moleculeStateMachine.ResetMolecule();
            Debug.Log("No available mRNA controllers are available");
            return;
        }

   

        //move it to molecule object
        newController.transform.position = moleculeStateMachine.transform.position;

        //enable object
        newController.gameObject.SetActive(true);
        newController.Activate(this);

        //attach handle to molecule
        newController.handle.transform.SetParent(moleculeStateMachine.modelParent, false);

        //remove from list
        mRNAControllers.Remove(newController);
        activeControllers.Add(newController);

        //trigger growth
        RNApolymeraseController controller = moleculeStateMachine.GetComponent<RNApolymeraseController>();
        if (controller != null)
            controller.SetmRNA(newController, mol);

        RibosomeController ribosome = moleculeStateMachine.GetComponent<RibosomeController>();
        if (ribosome != null)
        {
            newController.handle.transform.SetParent(ribosome.mRNAExit, false);
            ribosome.SetmRNA(newController, mol);
        }
            
    }

    private mRNAController AvailableController()
    {
        if (mRNAControllers.Count > 0)
            return mRNAControllers[0];
        else if (RepurposeActiveController() != null)
            return RepurposeActiveController();
        else
            return null;
    }

    private mRNAController RepurposeActiveController()
    {
        foreach (mRNAController mRNA in activeControllers)
        {
            if (mRNA.ActiveAndAvailable())
            {
                GameObject molecule = mRNA.handle.GetComponentInParent<MoleculeStateMachine>().gameObject;
                DeactivatemRNA(mRNA);
                Destroy(molecule);
                return mRNA;
            }
                
        }
    
        return null;
    }

    private mRNAController ActivateNewController()
    {

        return null;
    }

    public void DeactivatemRNA(mRNAController mRNA)
    {
        mRNA.transform.SetParent(null, false);

        //replace handle
        mRNA.handle.transform.SetParent(mRNA.transform, false);
        mRNA.gameObject.SetActive(false);
        mRNA.isGrowing = false;
        mRNAControllers.Add(mRNA);
        activeControllers.Remove(mRNA);
    }
}

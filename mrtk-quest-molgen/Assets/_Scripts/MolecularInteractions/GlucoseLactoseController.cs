﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlucoseLactoseController : MonoBehaviour {

    public KeyCode addGlucose;
    public KeyCode removeGlucose;
    public KeyCode addLactose;
    public KeyCode removeLactose;


    private void FixedUpdate()
    {
        if(Input.GetKeyUp(addGlucose))
        {
            MoleculeManager.Instance.AddGlucose();
        }
        else if(Input.GetKeyUp(removeGlucose))
        {
            MoleculeManager.Instance.RemoveGlucose();
        }

        if (Input.GetKeyUp(addLactose))
        {
            MoleculeManager.Instance.AddLactose();
        }
        else if (Input.GetKeyUp(removeLactose))
        {
            MoleculeManager.Instance.RemoveLactose();
            Debug.Log("Called remove lactose");
        }
    }

    public void addG(){
        MoleculeManager.Instance.AddGlucose();
    }
    public void addL(){
        MoleculeManager.Instance.AddLactose();
    }
    public void remG(){
        MoleculeManager.Instance.RemoveGlucose();
    }
    public void remL(){
        MoleculeManager.Instance.RemoveLactose();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyToCamera : MonoBehaviour
{
    new public GameObject mc;
    new public AudioClip woosh;
    new public Vector3 mcPos;
    new public Vector3 dir;
    new public float dist;
    private bool sendable = false;
    private bool sending = false;
    new public float bubble = 1f;
    new public float flyspeed = 10f;
    new public float clicktime = 0.5f;
    protected Rigidbody r;
    protected AudioSource a;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Rigidbody>();
        a = GetComponent<AudioSource>();
        mc = GameObject.FindGameObjectWithTag("MainCamera");
        
        //StartCoroutine(FlyToCamCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        /*
        mcPos = mc.transform.position;
        dir = (this.transform.position - mc.transform.position).normalized;
        dist = Vector3.Distance(this.transform.position, mc.transform.position);
        */
        //Debug.DrawLine (mcPos, mcPos + dir * 10, Color.red, Mathf.Infinity);
        //Debug.Log("Distance to camera is "+ dist);
        /*
        if (dist > 1){
            r.AddForce(-dir*flyspeed);
        }
        else{
            r.velocity = Vector3.zero;
        }*/
    }
    public void ObjectSelected(){
        sending = false;
        dist = Vector3.Distance(this.transform.position, mc.transform.position);
        if (dist > (bubble*1.1)){
            StartCoroutine(ClickedCoroutine());
        }
    }

    public void ObjectDeselected()
    {
        StartCoroutine(FlyToCamCoroutine());
    }
    /*
    void StartFlying(){
        if (dist > 1){
            r.AddForce(-dir*flyspeed);
            StartFlying();
        }
        else{
            r.velocity = Vector3.zero;
        }
    }
    */
    IEnumerator ClickedCoroutine()
    {
        sendable = true;
        yield return new WaitForSeconds(clicktime);
        sendable = false;
    }
    IEnumerator FlyToCamCoroutine()
    {
        if(sendable){
            mcPos = mc.transform.position;
            dist = Vector3.Distance(this.transform.position, mc.transform.position);
            sending = true;
            a.PlayOneShot(woosh);
            while ((dist > bubble) && sending){
                mcPos = mc.transform.position;
                dir = (this.transform.position - mc.transform.position).normalized;
                dist = Vector3.Distance(this.transform.position, mc.transform.position);
                //Print the time of when the function is first called.
                Debug.Log("Started FlyingCoroutine at timestamp : " + Time.time);
                r.AddForce(-dir*flyspeed);
                //yield on a new YieldInstruction that waits for 5 seconds.
                yield return new WaitForSeconds(0.1f);
            /*
            else{
                r.velocity = Vector3.zero;
            }
            */
            }
            sending = false;
            //After we have waited 5 seconds print the time again.
            Debug.Log("Stopped Flying Coroutine at timestamp : " + Time.time);
            r.velocity = Vector3.zero;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class becomeKin : MonoBehaviour
{
    public Rigidbody rb;
    public MoleculeStateMachine msm;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        msm = GetComponentInParent<MoleculeStateMachine>();
        StartCoroutine(ExampleCoroutine());
    }
    public void setKinTrue(){
        StartCoroutine(LaterKinCoroutine());
    }public void setKinFalse(){
        rb.isKinematic = false;
    }
    IEnumerator ExampleCoroutine()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(5);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
        rb.isKinematic = true;
    }
    IEnumerator LaterKinCoroutine()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(1);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
        rb.isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        //rb.isKinematic = true;
    }
}

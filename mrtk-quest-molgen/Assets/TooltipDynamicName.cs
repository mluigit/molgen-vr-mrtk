﻿using System.Collections;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections.Generic;
using UnityEngine;
public class TooltipDynamicName : MonoBehaviour
{
    private ToolTip tt;
    public GameObject molecule;
    public string nameString = "";
    
    
    // Start is called before the first frame update
    void Start()
    {
        tt = FindObjectOfType<ToolTip>();
        nameString = molecule.name;
        tt.ToolTipText = nameString;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToController : MonoBehaviour
{

    public GameObject leftController;
    public GameObject mc;
    public bool menuOpen = false;
    // Start is called before the first frame update
    void Start()
    {
        mc = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MenuButtonPressed(){
        if(!menuOpen){
            MoveToHand();
        }
        else{
            menuOpen = false;
            gameObject.SetActive(false);
        }
    }

    public void MoveToHand()
    {
        leftController = GameObject.Find("LeftHandAnchor");
        transform.position = leftController.transform.position;
        transform.LookAt(mc.transform);
        menuOpen = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//<summary>
//This class contains the score of the current scene. Everytime 
//the score needs to be update AddScore() should be called.
//</summary>
public class ScoreManager : MonoBehaviour
{
    
    public float completeScore;

    private float currentScore = 0;
    private bool isCompleted = false;

    private LevelManager levelManager;


    void Awake()
    { 
        // Find the levelmanager and add this scene to its SceneList.
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();

        levelManager.SetActiveScene();
        levelManager.SetSceneScore(this.gameObject);
        levelManager.UpdateScenesList();
       
    }

    // Add the float amount add to the currentScore and change the level
    // status to completed if currentScore matches the completedScore.
    public void AddScore(float add)
    {
        currentScore += add;
        if (currentScore >= completeScore)
        {
            SetCompletionStatus(true);
            currentScore = completeScore;
        }

        levelManager.UpdateScenesList();
    }

    // Return the current score
    public float GetScore()
    {
        return currentScore;
    }

    // Changes the completion status
    private void SetCompletionStatus(bool status)
    {
        isCompleted = status;
    }

}

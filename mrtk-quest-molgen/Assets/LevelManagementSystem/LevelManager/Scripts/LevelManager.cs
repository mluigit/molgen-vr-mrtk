﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using Valve.VR;
//using Valve.VR.InteractionSystem;

//<summary>
//This class stores the name of every scene in a dictionary along with the score within
//each of those scenes. There can only be one instance of this class during runtime across
//all scenes.When a the score of a scene changes it must call the UpdateSceneList() method to
//have it's new score added.
//</summary>

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    private Scene activeScene;
    public Dictionary<string, float> sceneScores = new Dictionary<string, float>();


    private void Awake()
    {
        //SIngleton - Ensuring there is only one instance of this object at all times.
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    //Adds the name of the active scene as a key to the dictionary scenesList with 
    //the score of that scene stored as the value. 
    public void UpdateScenesList()
    {
        string sceneName = activeScene.name;
        float score = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>().GetScore();

        if (sceneScores.ContainsKey(sceneName))
        {
            sceneScores[sceneName] = score;

        }
        else
        {
            sceneScores.Add(sceneName, score);

        }
    }


    public void SetActiveScene()
    {
        activeScene = SceneManager.GetActiveScene();
    }
    
    //Sets the stored score of the loaded scene as the current score
    //of the scoreManager in that scene.
    public void SetSceneScore(GameObject scoreManager)
    {
        if (sceneScores.ContainsKey(activeScene.name))
        {
            float sceneScore = sceneScores[activeScene.name];
            scoreManager.GetComponent<ScoreManager>().AddScore(sceneScore);
        }
        
    }

    public void LoadScene(string name)
    {
        //SteamVR_LoadLevel.Begin(name);
        //SteamVR_Fade.View(Color.black, 0.5f);
        //StartCoroutine(WaitForFade(0.5f, name));
    }
    
    IEnumerator WaitForFade(float duration, string name)
    {
        yield return new WaitForSeconds(duration);
        //SceneManager.LoadScene(name);
        
    }

    //event when scene is loaded
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //SteamVR_Fade.View(Color.clear, 1f);
    }
}

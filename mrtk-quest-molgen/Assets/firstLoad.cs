﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firstLoad : MonoBehaviour
{
    public bool loaded = false ;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitCoroutine());
    }
    IEnumerator WaitCoroutine()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(5);
        loaded = true;
        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }
}
